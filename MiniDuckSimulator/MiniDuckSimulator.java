public class MiniDuckSimulator {
    public static void main(String[] args) {
        System.out.println("Hello World");
        Duck duck = new MallardDuck();
        duck.setFlyBehavior(new FlyWithWings());
        duck.setQuackBehavior(new Quack());

        duck.performFly();
        duck.performQuack();

        Duck model = new ModelDuck();
        model.setFlyBehavior(new FlyRocketPowered());
        model.setQuackBehavior(new MuteQuack());

        model.performFly();
        model.performQuack();
    }
}
